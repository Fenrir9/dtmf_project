function tru = is_noise_between_symbol(sym, count, type)
% funkcja sprawdzajaca czy wystapil szum 
if type == 'l'
    if((count > 6 && sym(1, count) == 0 && sym(1, count-1) == 0 && sym(1, count-2) == 0 && sym(1, count-3) == 1 && sym(1, count-4) == 1 && sym(1, count-5) == 1) == 1)
        tru = 1;
    else
        tru = 0;
    end
elseif type == 'h'
    if((count > 6 && sym(5, count) == 0 && sym(5, count-1) == 0 && sym(5, count-2) == 0 && sym(5, count-3) == 1 && sym(5, count-4) == 1 && sym(5, count-5) == 1) == 1)
        tru = 1;
    else
        tru = 0;
    end
end
    
    