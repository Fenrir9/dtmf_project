function tresh = find_threshold(s, c, type)
% funkcja obliczajaca nowy tresold na podstawie niedawnego sygnalu
sum = 0;
temporary = 0;
if type == 'l'
    while( s(1,c-temporary) == 1)
        sum = sum + s(3,c-temporary);
        temporary = temporary + 1;
    end
elseif type == 'h'
    while( s(5,c-temporary) == 1)
        sum = sum + s(7,c-temporary);
        temporary = temporary + 1;
    end
end
tresh = sum / temporary;

