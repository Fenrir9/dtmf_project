function [lfrequency, noise] = lf_symbol(data_to_search, fs)
% funkcja filtrujaca zadany sygnal, wyznaczajaca ten o najwiekszej mocy
% (najwiekszej amplitudzie) oraz obliczajaca szum z pozostalych sygnalow

Wlf1 = [692/(fs/2), 702/(fs/2)]; % pass frquency 697Hz
Wlf2 = [765/(fs/2), 775/(fs/2)]; % pass frequency 770Hz
Wlf3 = [847/(fs/2), 857/(fs/2)]; % pass frequency 852Hz
Wlf4 = [936/(fs/2), 946/(fs/2)]; % pass frequency 941Hz


[b1, a1] = ellip(3,0.1,60,Wlf1,'bandpass'); %coeficients of 697 frequency filter
[b2, a2] = ellip(3,0.1,60,Wlf2,'bandpass'); %coeficients of 770 frequency filter
[b3, a3] = ellip(3,0.1,60,Wlf3,'bandpass'); %coeficients of 852 frequency filter
[b4, a4] = ellip(3,0.1,60,Wlf4,'bandpass'); %coeficients of 941 frequency filter
%{
[b1, a1] = butter(3,Wlf1,'bandpass'); %coeficients of 697 frequency filter
[b2, a2] = butter(3,Wlf2,'bandpass'); %coeficients of 770 frequency filter
[b3, a3] = butter(3,Wlf3,'bandpass'); %coeficients of 852 frequency filter
[b4, a4] = butter(3,Wlf4,'bandpass'); %coeficients of 941 frequency filter
%}
data_697 = filter(b1,a1,data_to_search); %filtered fdata
data_770 = filter(b2,a2,data_to_search);
data_852 = filter(b3,a3,data_to_search);
data_941 = filter(b4,a4,data_to_search);

sum_data_697 = sum(abs(data_697));
sum_data_770 = sum(abs(data_770));
sum_data_852 = sum(abs(data_852));
sum_data_941 = sum(abs(data_941));


symbol = [sum_data_697, sum_data_770, sum_data_852, sum_data_941];

[value, lfrequency] = max(symbol);

sorted_noise = sort(symbol, 'ascend');

noise = sum(sorted_noise(1:3)) *1.8; %1.7
