function [is_symbol] = if_symbol(data_to_be_processed, thresh)
% funkcja sprawdzajaca czy w zadanym elemencie sygnalu jesst symbol czy
% szum
c = sum(abs(data_to_be_processed));
if c > thresh
    c;
    is_symbol = 1;
else
    is_symbol = 0;
end