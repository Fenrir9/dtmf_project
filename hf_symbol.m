function [hfrequency, noise] = hf_symbol(data_to_search, fs)
% funkcja filtrujaca zadany sygnal, wyznaczajaca ten o najwiekszej mocy
% (najwiekszej amplitudzie) oraz obliczajaca szum z pozostalych sygnalow

wavelength = 5;  
Whf1 = [(1209-wavelength)/(fs/2), (1209+wavelength)/(fs/2)]; % pass frquency 1209Hz
Whf2 = [(1336-wavelength)/(fs/2), (1336+wavelength)/(fs/2)]; % pass frequency 1336Hz
Whf3 = [(1477-wavelength)/(fs/2), (1477+wavelength)/(fs/2)]; % pass frequency 1477Hz


[b1, a1] = ellip(3,0.1,60,Whf1,'bandpass'); %coeficients of 1209 frequency filter
[b2, a2] = ellip(3,0.1,60,Whf2,'bandpass'); %coeficients of 1336 frequency filter
[b3, a3] = ellip(3,0.1,60,Whf3,'bandpass'); %coeficients of 1477 frequency filter

%{
[b1, a1] = butter(3,Whf1,'bandpass'); %coeficients of 1209 frequency filter
[b2, a2] = butter(3,Whf2,'bandpass'); %coeficients of 1336 frequency filter
[b3, a3] = butter(3,Whf3,'bandpass'); %coeficients of 1477 frequency filter
%}

data_1209 = filter(b1,a1,data_to_search); %filtered fdata
data_1336 = filter(b2,a2,data_to_search);
data_1477 = filter(b3,a3,data_to_search);

sum_data_1209 = sum(abs(data_1209));
sum_data_1336 = sum(abs(data_1336));
sum_data_1477 = sum(abs(data_1477));

symbol = [sum_data_1209, sum_data_1336, sum_data_1477];

[value, hfrequency] = max(symbol);

sorted_noise = sort(symbol, 'ascend');

noise = sum(sorted_noise(1:2))*6;