function decoded_signal = decod(encoded_signal)
% funkcja przeksztalcajaca numery odpowieadajace czestotliwoscia w znaki z
% "klawiatury"
le = max(length(encoded_signal));

decoded_signal = "";


for x = 1:le
    if encoded_signal(1,x) == 1
        if encoded_signal(2,x) == 1
            decoded_signal = decoded_signal + '1';
        elseif encoded_signal(2,x) == 2
            decoded_signal = decoded_signal + '2';
        elseif encoded_signal(2,x) == 3
            decoded_signal = decoded_signal + '3';
        end
    elseif encoded_signal(1,x) == 2
        if encoded_signal(2,x) == 1
            decoded_signal = decoded_signal + '4';
        elseif encoded_signal(2,x) == 2
            decoded_signal = decoded_signal + '5';
        elseif encoded_signal(2,x) == 3
            decoded_signal = decoded_signal + '6';
        end
    elseif encoded_signal(1,x) == 3
        if encoded_signal(2,x) == 1
            decoded_signal = decoded_signal + '7';
        elseif encoded_signal(2,x) == 2
            decoded_signal = decoded_signal + '8';
        elseif encoded_signal(2,x) == 3
            decoded_signal = decoded_signal + '9';
        end
    elseif encoded_signal(1,x) == 4
        if encoded_signal(2,x) == 1
            decoded_signal = decoded_signal + '*';
        elseif encoded_signal(2,x) == 2
            decoded_signal = decoded_signal + '0';
        elseif encoded_signal(2,x) == 3
            decoded_signal = decoded_signal + '#';
        end
    end
end
            

