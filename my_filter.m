function filtered_signal = my_filter(data_to_be_processed, fs, type)
% funkcja filtrujaca sygnal

L = length(data_to_be_processed);
s = (1:L);
f = fs*(1:(L))/L;
f = transpose(f);

if type == 'l'

Wlf1 = [692/(fs/2), 702/(fs/2)]; % pass frquency 697Hz
Wlf2 = [765/(fs/2), 775/(fs/2)]; % pass frequency 770Hz
Wlf3 = [847/(fs/2), 857/(fs/2)]; % pass frequency 852Hz
Wlf4 = [936/(fs/2), 946/(fs/2)]; % pass frequency 941Hz

[b1, a1] = ellip(3,0.1,60,Wlf1,'bandpass'); %coeficients of 697 frequency filter
[b2, a2] = ellip(3,0.1,60,Wlf2,'bandpass'); %coeficients of 770 frequency filter
[b3, a3] = ellip(3,0.1,60,Wlf3,'bandpass'); %coeficients of 852 frequency filter
[b4, a4] = ellip(3,0.1,60,Wlf4,'bandpass'); %coeficients of 941 frequency filter
%{
[b1, a1] = butter(3,Wlf1,'bandpass'); %coeficients of 697 frequency filter
[b2, a2] = butter(3,Wlf2,'bandpass'); %coeficients of 770 frequency filter
[b3, a3] = butter(3,Wlf3,'bandpass'); %coeficients of 852 frequency filter
[b4, a4] = butter(3,Wlf4,'bandpass'); %coeficients of 941 frequency filter
%}

data_692 = filter(b1,a1,data_to_be_processed); %filtered fdata
data_770 = filter(b2,a2,data_to_be_processed);
data_852 = filter(b3,a3,data_to_be_processed);
data_941 = filter(b4,a4,data_to_be_processed);

filtered_signal = data_692+data_770+data_852+data_941;

spectrum_692 = fft(data_692); %spectrum
spectrum_770 = fft(data_770);
spectrum_852 = fft(data_852);
spectrum_941 = fft(data_941);

% wykresy do podejzenia
%{
figure(12)
grid on;
subplot(1,2,1)
plot(data_to_be_processed)
title('dane wejsciowe')
subplot(1,2,2)
plot(f,real(fft(data_to_be_processed)))
title('widmo sygnalu wejsciowego')
grid on;


figure(22)
subplot(1,2,1)
plot(s,data_692)
title('dane filtrowane dla niskich czestotliwosci 692')
subplot(1,2,2)
plot(f,real(spectrum_692));
title('widmo sygnalu odfiltrowanego dla niskich czestotliwosci')


figure(32)
subplot(1,2,1)
plot(s,data_770)
title('dane filtrowane dla niskich czestotliwosci 770')
subplot(1,2,2)
plot(f,real(spectrum_770));
title('widmo sygnalu odfiltrowanego dla niskich czestotliwosci')

figure(42)
subplot(1,2,1)
plot(s,data_852)
title('dane filtrowane dla niskich czestotliwosci 852')
subplot(1,2,2)
plot(f,real(spectrum_852));
title('widmo sygnalu odfiltrowanego dla niskich czestotliwosci')

figure(52)
subplot(1,2,1)
plot(s,data_941)
title('dane filtrowane dla niskich czestotliwosci 941')
subplot(1,2,2)
plot(f,real(spectrum_941));
title('widmo sygnalu odfiltrowanego dla niskich czestotliwosci')

figure(62)
subplot(1,2,1)
plot(s,filtered_signal)
title('suma danych odfiltrowanych dla niskich czestotliwosci')
subplot(1,2,2)
plot(f,real(fft(filtered_signal)));
title('widmo sygnalu odfiltrowanego dla niskich czestotliwosci')
%}
elseif type == 'h'

wavelength = 5;
Whf1 = [(1209-wavelength)/(fs/2), (1209+wavelength)/(fs/2)]; % pass frquency 1209Hz
Whf2 = [(1336-wavelength)/(fs/2), (1336+wavelength)/(fs/2)]; % pass frequency 1336Hz
Whf3 = [(1477-wavelength)/(fs/2), (1477+wavelength)/(fs/2)]; % pass frequency 1477Hz

[b1, a1] = ellip(3,0.1,60,Whf1,'bandpass'); %coeficients of 1209 frequency filter
[b2, a2] = ellip(3,0.1,60,Whf2,'bandpass'); %coeficients of 1336 frequency filter
[b3, a3] = ellip(3,0.1,60,Whf3,'bandpass'); %coeficients of 1477 frequency filter
%{
[b1, a1] = butter(3,Whf1,'bandpass'); %coeficients of 697 frequency filter
[b2, a2] = butter(3,Whf2,'bandpass'); %coeficients of 770 frequency filter
[b3, a3] = butter(3,Whf3,'bandpass'); %coeficients of 852 frequency filter
%}


data_1209 = filter(b1,a1,data_to_be_processed); %filtered fdata
data_1336 = filter(b2,a2,data_to_be_processed);
data_1477 = filter(b3,a3,data_to_be_processed);

filtered_signal = data_1209+data_1336+data_1477;

spectrum_1209 = fft(data_1209); %spectrum
spectrum_1336 = fft(data_1336);
spectrum_1477 = fft(data_1477);

%{
figure(92)
subplot(1,2,1)
plot(s,data_1209)
title('dane filtrowane dla wysokich czestotliwosci 1209')
subplot(1,2,2)
plot(f,real(spectrum_1209));
title('widmo sygnalu odfiltrowanego dla wysokich czestotliwosci')


figure(102)
subplot(1,2,1)
plot(s,data_1336)
title('dane filtrowane dla wysokich czestotliwosci 1336')
subplot(1,2,2)
plot(f,real(spectrum_1336));
title('widmo sygnalu odfiltrowanego dla wysokich czestotliwosci')

figure(112)
subplot(1,2,1)
plot(s,data_1477)
title('dane filtrowane dla wysokich czestotliwosci 1477')
subplot(1,2,2)
plot(f,real(spectrum_1477));
title('widmo sygnalu odfiltrowanego dla wysokich czestotliwosci')

figure(122)
subplot(1,2,1)
plot(s,filtered_signal)
title('suma danych odfiltrowanych dla wyskokich czestotliwosci')
subplot(1,2,2)
plot(f,real(fft(filtered_signal)));
title('widmo sygnalu odfiltrowanego dla wysokich czestotliwosci')
%}

end