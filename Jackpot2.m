clear all;
close all;

[data, Fs] = audioread('challenge9.wav');
 
% for low frequencies
lf_filtered_data = my_filter(data, Fs, 'l');

threshold_lf = 38;
investigated_part = ones(1,6000);
counter = 0;

hop = 100;
symbols = zeros(7, ceil(length(data)/hop));

for i = 1:hop:(length(data) - length(investigated_part))
    counter = counter + 1;
    investigated_part = ones(1,6000);
    investigated_part = lf_filtered_data(i:i+length(investigated_part));
    if if_symbol(investigated_part, threshold_lf) == 1
        symbols(1, counter) = 1;
        [symbols(2,counter), symbols(3, counter)] = lf_symbol(investigated_part,Fs);
    else
        symbols(1, counter) = 0;
        symbols(2,counter) = 0; 
        symbols(3, counter) = 0;
    end
    
    if is_noise_between_symbol(symbols, counter, 'l') == 1
        threshold_lf = find_threshold(symbols, counter - 3, 'l');
    end
end

% for high frequencies

hf_filtered_data = my_filter(data, Fs, 'h');

threshold_hf = 45; %12
investigated_part = ones(1,6000);
counter = 0;

for i = 1:hop:(length(data) - length(investigated_part))
    counter = counter + 1;
    investigated_part = ones(1,6000);
    investigated_part = hf_filtered_data(i:i+length(investigated_part));
    if if_symbol(investigated_part, threshold_hf) == 1
        symbols(5, counter) = 1;
        [symbols(6,counter), symbols(7, counter)] = hf_symbol(investigated_part,Fs);
    else
        symbols(4, counter) = 0;
        symbols(5,counter) = 0; 
        symbols(6, counter) = 0;
    end
    
    if is_noise_between_symbol(symbols, counter, 'h') == 1
        threshold_hf = find_threshold(symbols, counter - 3, 'h');
    end
end

encoded_symbols = appoint_symbol(symbols);

decoded_symbols = decod(encoded_symbols)




